package ru.t1.azarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.endpoint.IUserEndpoint;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.dto.request.user.*;
import ru.t1.azarin.tm.dto.response.user.UserRegistryResponse;
import ru.t1.azarin.tm.dto.response.user.UserUpdateProfileResponse;
import ru.t1.azarin.tm.marker.IntegrationCategory;
import ru.t1.azarin.tm.service.PropertyService;

import java.sql.SQLException;

@Category(IntegrationCategory.class)
public final class UserEndpointTest {

    @NotNull
    private static final IPropertyService PROPERTY_SERVICE = new PropertyService();

    @NotNull
    private static final IAuthEndpoint AUTH_ENDPOINT = IAuthEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final IUserEndpoint USER_ENDPOINT = IUserEndpoint.newInstance(PROPERTY_SERVICE);

    @NotNull
    private static final String USER1_LOGIN = "test_user";

    @NotNull
    private static final String USER1_PASSWORD = "test_user";

    @NotNull
    private static final String ADMIN_LOGIN = "admin";

    @NotNull
    private static final String ADMIN_PASSWORD = "admin";

    @Nullable
    private static String ADMIN_TOKEN;

    @NotNull
    private final String testString = "TEST_STRING";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @BeforeClass
    public static void setUp() throws SQLException {
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest();
        registryRequest.setLogin(USER1_LOGIN);
        registryRequest.setPassword(USER1_PASSWORD);
        USER_ENDPOINT.registryResponse(registryRequest);

        ADMIN_TOKEN = AUTH_ENDPOINT.login(new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD)).getToken();
    }

    @AfterClass
    public static void tearDown() throws SQLException {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(ADMIN_TOKEN);
        removeRequest.setLogin(USER1_LOGIN);
        USER_ENDPOINT.removeResponse(removeRequest);

        @NotNull final UserLogoutRequest request = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(request);
    }

    @Test
    public void changePassword() throws SQLException {
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.changePasswordResponse(new UserChangePasswordRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.changePasswordResponse(new UserChangePasswordRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.changePasswordResponse(new UserChangePasswordRequest(nullString))
        );
        @NotNull final UserChangePasswordRequest changePasswordRequest = new UserChangePasswordRequest(ADMIN_TOKEN);
        changePasswordRequest.setNewPassword(testString);
        USER_ENDPOINT.changePasswordResponse(changePasswordRequest);
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(request);
        Assert.assertNotNull(AUTH_ENDPOINT.login(new UserLoginRequest(ADMIN_LOGIN, testString)).getToken());
        @NotNull final UserChangePasswordRequest changePasswordRequest1 = new UserChangePasswordRequest(ADMIN_TOKEN);
        changePasswordRequest1.setNewPassword(ADMIN_PASSWORD);
        USER_ENDPOINT.changePasswordResponse(changePasswordRequest1);
    }

    @Test
    public void lockUser() throws SQLException {
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.lockResponse(new UserLockRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.lockResponse(new UserLockRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.lockResponse(new UserLockRequest(nullString))
        );
        @NotNull final UserLockRequest lockRequest = new UserLockRequest(ADMIN_TOKEN);
        lockRequest.setLogin(USER1_LOGIN);
        USER_ENDPOINT.lockResponse(lockRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(logoutRequest);
        Assert.assertThrows(Exception.class, () ->
                AUTH_ENDPOINT.login(new UserLoginRequest(AUTH_ENDPOINT.login(new UserLoginRequest(USER1_LOGIN, USER1_PASSWORD)).getToken()))
        );
    }

    @Test
    public void unlockUser() throws SQLException {
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.unlockResponse(new UserUnlockRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.unlockResponse(new UserUnlockRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.unlockResponse(new UserUnlockRequest(nullString))
        );
        @NotNull final UserUnlockRequest unlockRequest = new UserUnlockRequest(ADMIN_TOKEN);
        unlockRequest.setLogin(USER1_LOGIN);
        USER_ENDPOINT.unlockResponse(unlockRequest);
        @NotNull final UserLogoutRequest logoutRequest = new UserLogoutRequest(ADMIN_TOKEN);
        AUTH_ENDPOINT.logout(logoutRequest);
        Assert.assertNotNull(AUTH_ENDPOINT.login(new UserLoginRequest(USER1_LOGIN, USER1_PASSWORD)));
    }

    @Test
    public void registry() throws SQLException {
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.registryResponse(new UserRegistryRequest(emptyString, testString, testString))
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.registryResponse(new UserRegistryRequest(testString, emptyString, testString))
        );
        @NotNull final UserRegistryRequest request = new UserRegistryRequest();
        request.setLogin(testString);
        request.setPassword(testString);
        request.setEmail(testString);
        @Nullable final UserRegistryResponse response = USER_ENDPOINT.registryResponse(request);
        Assert.assertNotNull(response.getUser());
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(ADMIN_TOKEN);
        removeRequest.setLogin(testString);
        USER_ENDPOINT.removeResponse(removeRequest);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.login(new UserLoginRequest(testString, testString)));
    }

    @Test
    public void remove() throws SQLException {
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.removeResponse(new UserRemoveRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.removeResponse(new UserRemoveRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.removeResponse(new UserRemoveRequest(nullString))
        );
        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest();
        registryRequest.setLogin(testString);
        registryRequest.setPassword(testString);
        registryRequest.setEmail(testString);
        USER_ENDPOINT.registryResponse(registryRequest);
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(ADMIN_TOKEN);
        removeRequest.setLogin(testString);
        USER_ENDPOINT.removeResponse(removeRequest);
        Assert.assertThrows(Exception.class, () -> AUTH_ENDPOINT.login(new UserLoginRequest(testString, testString)));
    }

    @Test
    public void updateProfile() throws SQLException {
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.updateProfileResponse(new UserUpdateProfileRequest())
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.updateProfileResponse(new UserUpdateProfileRequest(emptyString))
        );
        Assert.assertThrows(Exception.class, () ->
                USER_ENDPOINT.updateProfileResponse(new UserUpdateProfileRequest(nullString))
        );
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(ADMIN_TOKEN);
        request.setFirstName(testString);
        request.setMiddleName(testString);
        request.setLastName(testString);
        @Nullable final UserUpdateProfileResponse response = USER_ENDPOINT.updateProfileResponse(request);
        Assert.assertEquals(testString, response.getUser().getFirstName());
        Assert.assertEquals(testString, response.getUser().getMiddleName());
        Assert.assertEquals(testString, response.getUser().getLastName());
    }

}
