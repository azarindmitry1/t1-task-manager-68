package ru.t1.azarin.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.model.UserDto;
import ru.t1.azarin.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.event.ConsoleEvent;

import java.sql.SQLException;

@Component
public final class UserViewProfileListener extends AbstractUserListener {

    @NotNull
    public final static String NAME = "user-view-profile";

    @NotNull
    public final static String DESCRIPTION = "View profile of current user.";

    @Override
    @EventListener(condition = "@userViewProfileListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[VIEW USER PROFILE]");
        @NotNull final UserViewProfileRequest request = new UserViewProfileRequest();
        @NotNull final UserDto user = authEndpoint.viewProfile(request).getUser();
        showUser(user);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
