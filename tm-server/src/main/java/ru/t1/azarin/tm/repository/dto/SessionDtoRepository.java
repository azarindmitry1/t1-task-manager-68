package ru.t1.azarin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.dto.model.SessionDto;

import java.util.List;

@Repository
@Scope("prototype")
public interface SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDto> {

    void deleteAllByUserId(@NotNull String userId);

    @Nullable
    List<SessionDto> findAllByUserId(@NotNull String userId);

    @Nullable
    SessionDto findByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
