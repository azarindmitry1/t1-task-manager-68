package ru.t1.dazarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.dazarin.tm.model.dto.ProjectDto;
import ru.t1.dazarin.tm.model.dto.soap.project.*;
import ru.t1.dazarin.tm.service.dto.ProjectDtoService;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class ProjectSoapEndpointImpl {

    @NotNull
    public static final String NAMESPACE = "http://dazarin.t1.ru/tm/model/dto/soap/project";

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectSoapEndpointPort";

    @NotNull
    @Autowired
    private ProjectDtoService projectService;

    @ResponsePayload
    @PayloadRoot(localPart = "ProjectCreateRequest", namespace = NAMESPACE)
    public ProjectCreateResponse create(@NotNull @RequestPayload ProjectCreateRequest request) {
        projectService.create();
        return new ProjectCreateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "ProjectFindByIdRequest", namespace = NAMESPACE)
    public ProjectFindByIdResponse findById(@NotNull @RequestPayload ProjectFindByIdRequest request) {
        @NotNull final ProjectFindByIdResponse response = new ProjectFindByIdResponse();
        @Nullable final ProjectDto project = projectService.findById(request.getId());
        response.setProject(project);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "ProjectFindAllRequest", namespace = NAMESPACE)
    public ProjectFindAllResponse findAll(@NotNull @RequestPayload ProjectFindAllRequest request) {
        @NotNull final ProjectFindAllResponse response = new ProjectFindAllResponse();
        @Nullable final List<ProjectDto> projects = new ArrayList<>(projectService.findAll());
        response.setProject(projects);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "ProjectSaveRequest", namespace = NAMESPACE)
    public ProjectSaveResponse save(@NotNull @RequestPayload final ProjectSaveRequest request) {
        projectService.save(request.getProject());
        return new ProjectSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "ProjectDeleteByIdRequest", namespace = NAMESPACE)
    public ProjectDeleteByIdResponse deleteById(@NotNull @RequestPayload ProjectDeleteByIdRequest request) {
        projectService.deleteById(request.getId());
        return new ProjectDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "ProjectDeleteAllRequest", namespace = NAMESPACE)
    public ProjectDeleteAllResponse deleteAll(@NotNull @RequestPayload ProjectDeleteAllRequest request) {
        projectService.deleteAll();
        return new ProjectDeleteAllResponse();
    }

}
