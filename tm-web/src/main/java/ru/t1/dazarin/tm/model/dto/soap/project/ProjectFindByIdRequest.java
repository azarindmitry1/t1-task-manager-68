package ru.t1.dazarin.tm.model.dto.soap.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "id"
})
@XmlRootElement(name = "ProjectFindByIdRequest")
public class ProjectFindByIdRequest {

    @XmlElement(required = true)
    protected String id;

}
