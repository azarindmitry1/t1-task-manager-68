package ru.t1.dazarin.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dazarin.tm.model.dto.ProjectDto;
import ru.t1.dazarin.tm.service.dto.ProjectDtoService;

import javax.jws.WebParam;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/project")
public class ProjectRestEndpointImpl implements IProjectEndpoint {

    private final ProjectDtoService projectDtoService;

    @Override
    @GetMapping(value = "/findAll")
    public List<ProjectDto> findAll() {
        return projectDtoService.findAll();
    }

    @Override
    @GetMapping(value = "/findById/{id}")
    public ProjectDto findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") final String id
    ) {
        return projectDtoService.findById(id);
    }

    @Override
    @PostMapping(value = "/create")
    public ProjectDto create() {
        return projectDtoService.create();
    }

    @Override
    @DeleteMapping(value = "/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") final String id
    ) {
        projectDtoService.deleteById(id);
    }

    @Override
    @DeleteMapping(value = "/deleteAll")
    public void deleteAll() {
        projectDtoService.deleteAll();
    }

    @Override
    @PutMapping(value = "/update")
    public ProjectDto update(
            @WebParam(name = "projectDto", partName = "projectDto")
            @NotNull @RequestBody final ProjectDto projectDto
    ) {
        return projectDtoService.save(projectDto);
    }

}
