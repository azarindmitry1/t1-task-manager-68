package ru.t1.dazarin.tm.model.dto.soap.project;

import ru.t1.dazarin.tm.model.dto.ProjectDto;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public ObjectFactory() {
    }

    public ProjectCreateRequest createProjectCreateRequest() {
        return new ProjectCreateRequest();
    }

    public ProjectCreateResponse createProjectCreateResponse() {
        return new ProjectCreateResponse();
    }

    public ProjectFindByIdRequest createProjectFindByIdRequest() {
        return new ProjectFindByIdRequest();
    }

    public ProjectFindByIdResponse createProjectFindByIdResponse() {
        return new ProjectFindByIdResponse();
    }

    public ProjectFindAllRequest createProjectFindAllRequest() {
        return new ProjectFindAllRequest();
    }

    public ProjectFindAllResponse createProjectFindAllResponse() {
        return new ProjectFindAllResponse();
    }

    public ProjectSaveRequest createProjectSaveRequest() {
        return new ProjectSaveRequest();
    }

    public ProjectSaveResponse createProjectSaveResponse() {
        return new ProjectSaveResponse();
    }

    public ProjectDeleteByIdRequest createProjectDeleteByIdRequest() {
        return new ProjectDeleteByIdRequest();
    }

    public ProjectDeleteByIdResponse createProjectDeleteByIdResponse() {
        return new ProjectDeleteByIdResponse();
    }

    public ProjectDeleteAllRequest createProjectDeleteAllRequest() {
        return new ProjectDeleteAllRequest();
    }

    public ProjectDeleteAllResponse createProjectDeleteAllResponse() {
        return new ProjectDeleteAllResponse();
    }

    public ProjectDto createProjectDto() {
        return new ProjectDto();
    }

}
