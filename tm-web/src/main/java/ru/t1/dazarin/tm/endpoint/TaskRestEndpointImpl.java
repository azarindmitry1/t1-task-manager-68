package ru.t1.dazarin.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.api.endpoint.ITaskEndpoint;
import ru.t1.dazarin.tm.model.dto.TaskDto;
import ru.t1.dazarin.tm.service.dto.TaskDtoService;

import javax.jws.WebParam;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/task")
public class TaskRestEndpointImpl implements ITaskEndpoint {

    private final TaskDtoService taskDtoService;

    @Override
    @GetMapping(value = "/findAll")
    public List<TaskDto> findAll() {
        return taskDtoService.findAll();
    }

    @Override
    @GetMapping(value = "/findById/{id}")
    public TaskDto findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") final String id
    ) {
        return taskDtoService.findById(id);
    }

    @Override
    @PostMapping(value = "/create")
    public TaskDto create() {
        return taskDtoService.create();
    }

    @Override
    @DeleteMapping(value = "/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") final String id
    ) {
        taskDtoService.deleteById(id);
    }

    @Override
    @DeleteMapping(value = "/deleteAll")
    public void deleteAll() {
        taskDtoService.deleteAll();
    }

    @Override
    @PutMapping(value = "/update")
    public TaskDto update(
            @WebParam(name = "taskDto", partName = "taskDto")
            @NotNull @RequestBody final TaskDto taskDto
    ) {
        return taskDtoService.save(taskDto);
    }

}
