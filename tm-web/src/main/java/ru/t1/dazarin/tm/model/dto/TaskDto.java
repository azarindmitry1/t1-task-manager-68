package ru.t1.dazarin.tm.model.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.format.annotation.DateTimeFormat;
import ru.t1.dazarin.tm.enumerated.Status;

import javax.persistence.*;
import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_task")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "taskDto", propOrder = {
        "id",
        "name",
        "description",
        "status",
        "createdDate",
        "dateStart",
        "dateFinish",
        "projectId"
})

public final class TaskDto {

    @Id
    @NotNull
    @XmlElement(required = true)
    private String id = UUID.randomUUID().toString();

    @Column
    @NotNull
    @XmlElement(required = true)
    private String name = "";

    @Column
    @NotNull
    @XmlElement
    private String description = "";

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    private Status status = Status.NOT_STARTED;

    @CreatedDate
    @Column(name = "created_at")
    @XmlElement(required = true)
    @XmlSchemaType(name = "createdDate")
    private Date createdDate = new Date();

    @XmlElement
    @Column(name = "date_start")
    @XmlSchemaType(name = "dateStart")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateStart;

    @XmlElement
    @Column(name = "date_finish")
    @XmlSchemaType(name = "dateFinish")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateFinish;

    @Nullable
    @XmlElement
    @Column(name = "project_id")
    private String projectId;

    public TaskDto(@NotNull final String name) {
        this.name = name;
    }

    public TaskDto(@NotNull final String name, @NotNull final String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " : " + description;
    }

}
