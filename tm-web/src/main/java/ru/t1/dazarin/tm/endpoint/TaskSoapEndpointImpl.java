package ru.t1.dazarin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.dazarin.tm.model.dto.TaskDto;
import ru.t1.dazarin.tm.model.dto.soap.task.*;
import ru.t1.dazarin.tm.service.dto.TaskDtoService;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class TaskSoapEndpointImpl {

    @NotNull
    public static final String NAMESPACE = "http://dazarin.t1.ru/tm/model/dto/soap/task";

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "TaskSoapEndpointPort";

    @NotNull
    @Autowired
    private TaskDtoService taskService;

    @ResponsePayload
    @PayloadRoot(localPart = "TaskCreateRequest", namespace = NAMESPACE)
    public TaskCreateResponse create(@NotNull @RequestPayload TaskCreateRequest request) {
        taskService.create();
        return new TaskCreateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "TaskFindByIdRequest", namespace = NAMESPACE)
    public TaskFindByIdResponse findById(@NotNull @RequestPayload TaskFindByIdRequest request) {
        @NotNull final TaskFindByIdResponse response = new TaskFindByIdResponse();
        @Nullable final TaskDto task = taskService.findById(request.getId());
        response.setTask(task);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "TaskFindAllRequest", namespace = NAMESPACE)
    public TaskFindAllResponse findAll(@NotNull @RequestPayload TaskFindAllRequest request) {
        @NotNull final TaskFindAllResponse response = new TaskFindAllResponse();
        @Nullable final List<TaskDto> tasks = new ArrayList<>(taskService.findAll());
        response.setTask(tasks);
        return response;
    }

    @ResponsePayload
    @PayloadRoot(localPart = "TaskSaveRequest", namespace = NAMESPACE)
    public TaskSaveResponse save(@NotNull @RequestPayload TaskSaveRequest request) {
        taskService.save(request.getTask());
        return new TaskSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "TaskDeleteByIdRequest", namespace = NAMESPACE)
    public TaskDeleteByIdResponse deleteById(@NotNull @RequestPayload TaskDeleteByIdRequest request) {
        taskService.deleteById(request.getId());
        return new TaskDeleteByIdResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "TaskDeleteAllRequest", namespace = NAMESPACE)
    public TaskDeleteAllResponse deleteAll(@NotNull @RequestPayload TaskDeleteAllRequest request) {
        taskService.deleteAll();
        return new TaskDeleteAllResponse();
    }

}
