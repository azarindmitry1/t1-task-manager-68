<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>EDIT TASK</h1>

<form:form action="/task/update/${task.id}" method="POST" modelAttribute="task">

    <form:input type="hidden" path="id" />

    <p>
        <div>
            <span>NAME:</span>
        </div>
        <div>
            <form:input type="text" path="name" />
        </div>
    </p>

    <p>
        <div>
            <span>DESCRIPTION:</span>
        </div>
        <div>
            <form:input type="text" path="description" />
        </div>
    </p>

    <p>
        <div>
            <span>PROJECT:</span>
        </div>
        <div>
            <form:select path="projectId">
                <form:option value="${null}" label="--- // ---" />
                <form:options items="${projects}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
    </p>

    <p>
        <div>
            <span>STATUS:</span>
        </div>
        <div>
            <form:select path="status">
                <form:option value="${null}" label="--- // ---" />
                <form:options items="${statuses}" itemLabel="displayName" />
            </form:select>
        </div>
    </p>

    <p>
        <div>
            <span>DATE BEGIN:</span>
        </div>
        <div>
            <form:input type="date" path="dateStart" />
        </div>
    </p>

    <p>
        <div>
            <span>DATE FINISH:</span>
        </div>
        <div>
            <form:input type="date" path="dateFinish" />
        </div>
    </p>

    <button type="submit">SAVE TASK</button>

</form:form>

<jsp:include page="../include/_footer.jsp"/>
