package ru.t1.dazarin.tm;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.dazarin.tm.client.TestClient;
import ru.t1.dazarin.tm.marker.IntegrationCategory;
import ru.t1.dazarin.tm.model.dto.TaskDto;

@Category(IntegrationCategory.class)
public class TestClientTest {

    private static final String BASEURL = "http://localhost:8080/api/task";

    private final TestClient testClient = TestClient.client(BASEURL);

    private TaskDto taskOne;

    private TaskDto taskTwo;

    @Before
    public void setUp() {
        taskOne = testClient.create();
        taskTwo = testClient.create();
    }

    @After
    public void tearDown() {
        testClient.deleteAll();
    }

    @Test
    public void findAll() {
        Assert.assertEquals(2, testClient.findAll().size());
    }

    @Test
    public void findById() {
        final TaskDto taskDto = testClient.findById(taskOne.getId());
        Assert.assertEquals(taskOne.getId(), taskDto.getId());
    }

    @Test
    public void create() {
        final TaskDto taskDto = testClient.create();
        Assert.assertEquals(taskDto.getId(), testClient.findById(taskDto.getId()).getId());
    }

    @Test
    public void deleteById() {
        testClient.deleteById(taskOne.getId());
        Assert.assertEquals(1, testClient.findAll().size());
    }

    @Test
    public void deleteAll() {
        testClient.deleteAll();
        Assert.assertEquals(0, testClient.findAll().size());
    }

    @Test
    public void update() {
        final TaskDto taskDto = testClient.findById(taskOne.getId());
        taskDto.setDescription("test update");
        testClient.update(taskDto);
        Assert.assertEquals("test update", testClient.findById(taskDto.getId()).getDescription());
    }

}
